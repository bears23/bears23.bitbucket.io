self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('co23').then(function(cache) {
     return cache.addAll([
       'index.html',
       'themes/bears.min.css',
       'jquery.mobile.structure-1.2.1.min.css',
       'jquery-1.8.3.min.js',
       'jquery.mobile-1.2.1.min.js'
     ]);
   })
 );
});
self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function(event) {
 console.log(event.request.url);

 event.respondWith(
   caches.match(event.request).then(function(response) {
     return response || fetch(event.request);
   })
 );
});
