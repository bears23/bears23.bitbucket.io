module.exports = {
  "globDirectory": ".",
  "globPatterns": [
    "**/*.{png,js,html,ico,css}"
  ],
  "swDest": "sw.js"
};