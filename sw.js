/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "android-chrome-144x144.png",
    "revision": "f5d09a8110b51150fa77eb8765ba5e1d"
  },
  {
    "url": "android-chrome-192x192.png",
    "revision": "9ee64e9d170487e1d26002bb29aca2d3"
  },
  {
    "url": "android-chrome-256x256.png",
    "revision": "1b841fc6c8d9fae8d0f36ec27896eac6"
  },
  {
    "url": "android-chrome-36x36.png",
    "revision": "164570d0fbfcba61fcf490112e905095"
  },
  {
    "url": "android-chrome-384x384.png",
    "revision": "e6fcaeadb553b87b3243f18149f9f1ab"
  },
  {
    "url": "android-chrome-48x48.png",
    "revision": "28c9f9a750bcaf98b4010fb6e801c286"
  },
  {
    "url": "android-chrome-512x512.png",
    "revision": "535e98f3026189cb2b29185058bac784"
  },
  {
    "url": "android-chrome-72x72.png",
    "revision": "e51ce0bd266b2fbb6fa7243d7b5bb7bf"
  },
  {
    "url": "android-chrome-96x96.png",
    "revision": "fcefd06030d3dc6d17552bed49a366c2"
  },
  {
    "url": "apple-touch-icon-114x114-precomposed.png",
    "revision": "5672e66813d8c2339d19bd6a180ed9c2"
  },
  {
    "url": "apple-touch-icon-114x114.png",
    "revision": "2ec40e5fd3884664c2c5417a9bd986aa"
  },
  {
    "url": "apple-touch-icon-120x120-precomposed.png",
    "revision": "dd470136c291339022d750c630c7cc95"
  },
  {
    "url": "apple-touch-icon-120x120.png",
    "revision": "6ea509970eae88d13304c0c1c5f18f69"
  },
  {
    "url": "apple-touch-icon-144x144-precomposed.png",
    "revision": "17a53b8e6f18533e5541c6cb950e68e3"
  },
  {
    "url": "apple-touch-icon-144x144.png",
    "revision": "44eee56b76281970258d2ab3a7424d81"
  },
  {
    "url": "apple-touch-icon-152x152-precomposed.png",
    "revision": "8d99d8855664abd05f1b5657ae6aba2e"
  },
  {
    "url": "apple-touch-icon-152x152.png",
    "revision": "a86ee6364535018f7b27fe8f6f4ce1c4"
  },
  {
    "url": "apple-touch-icon-180x180-precomposed.png",
    "revision": "5b5fd7b77650d540ec2495e0e52a527a"
  },
  {
    "url": "apple-touch-icon-180x180.png",
    "revision": "6ff7e6b0ce3daf5942acaf2e3c96ad2e"
  },
  {
    "url": "apple-touch-icon-57x57-precomposed.png",
    "revision": "2f4f7f3309f361a675e8998f86979f26"
  },
  {
    "url": "apple-touch-icon-57x57.png",
    "revision": "c0b11457436de2e6b14f2d84e7b18d02"
  },
  {
    "url": "apple-touch-icon-60x60-precomposed.png",
    "revision": "f7d69ba9972c944b793568fdf624318f"
  },
  {
    "url": "apple-touch-icon-60x60.png",
    "revision": "652fe37cd0f3a77dac1b6cdfb8a7c4af"
  },
  {
    "url": "apple-touch-icon-72x72-precomposed.png",
    "revision": "acdec6e6d7a5c7c563f6a33fd321aff9"
  },
  {
    "url": "apple-touch-icon-72x72.png",
    "revision": "04f50d75a4332d05fdb19a1becdc99f9"
  },
  {
    "url": "apple-touch-icon-76x76-precomposed.png",
    "revision": "957b72871724782e12dce670aa0692c2"
  },
  {
    "url": "apple-touch-icon-76x76.png",
    "revision": "a5e037df343b5ede904e863af1d27931"
  },
  {
    "url": "apple-touch-icon-precomposed.png",
    "revision": "5b5fd7b77650d540ec2495e0e52a527a"
  },
  {
    "url": "apple-touch-icon.png",
    "revision": "6ff7e6b0ce3daf5942acaf2e3c96ad2e"
  },
  {
    "url": "cptable.full.js",
    "revision": "6a08569b7648eed5eaef833b76cf1613"
  },
  {
    "url": "date.js",
    "revision": "693cbb2a3d4df31421d61b56e679f45a"
  },
  {
    "url": "demo.html",
    "revision": "0a2498849bfa3a069102ad56d97892ea"
  },
  {
    "url": "example.js",
    "revision": "2d452b473c5a496b329928fd944b0f66"
  },
  {
    "url": "example2.js",
    "revision": "70e57114da588ad1237fe836a847cc99"
  },
  {
    "url": "favicon-16x16.png",
    "revision": "ec874dc0f964d8e63c4c5a1e0dda8566"
  },
  {
    "url": "favicon-32x32.png",
    "revision": "fd039d89260fa60723386a324199da6c"
  },
  {
    "url": "favicon.ico",
    "revision": "026d564340a92be38fa289c14bb86b65"
  },
  {
    "url": "iconfinder_book_blue_61564.png",
    "revision": "33c1260da5f784b7de21d4f684def37a"
  },
  {
    "url": "images/icons-18-black.png",
    "revision": "f9e3c93059ef0fc1ca1ead499f19d489"
  },
  {
    "url": "images/icons-18-white.png",
    "revision": "b3da1f56006b8f830af6dde218d72185"
  },
  {
    "url": "images/icons-36-black.png",
    "revision": "24bde6ae0459e200a2b2ac290787022d"
  },
  {
    "url": "images/icons-36-white.png",
    "revision": "632ff9bef83adcf1cd745c938a4216af"
  },
  {
    "url": "index.html",
    "revision": "0c606ffc77b1f5b7b8fcb0fd98afcdc7"
  },
  {
    "url": "jquery-1.8.3.min.js",
    "revision": "3576a6e73c9dccdbbc4a2cf8ff544ad7"
  },
  {
    "url": "jquery.mobile-1.2.1.min.css",
    "revision": "5f96a23161e77f0ebcb6b221d2f461d7"
  },
  {
    "url": "jquery.mobile-1.2.1.min.js",
    "revision": "b92ac578539e13e7fb3f095d70d81f68"
  },
  {
    "url": "jquery.mobile.structure-1.2.1.min.css",
    "revision": "141998fe8faa94c6c5c1753467f0cd6f"
  },
  {
    "url": "link.html",
    "revision": "6278c418816b2344dee997e13f8df63d"
  },
  {
    "url": "mstile-144x144.png",
    "revision": "074b15dbccd0b07a3a75c7e2f274b744"
  },
  {
    "url": "mstile-150x150.png",
    "revision": "34c132d2a6055cd1cd26101521641b20"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
